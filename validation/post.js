const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validatePostInput(data) {
	let errors = {};

	data.text = !isEmpty(data.text) ? data.text : '';

	if (!Validator.isLength(data.text, { min: 3 })) {
		errors.text = 'Text must have minimum 3 characters.';
	}

	if (Validator.isEmpty(data.text)) {
		errors.text = 'This field is required';
	}

	return {
		errors,
		isValid: isEmpty(errors)
	};
};
