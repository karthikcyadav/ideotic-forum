import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AnswerItem from './AnswerItem';

class AnswerFeed extends Component {
	render() {
		const { answers, postId } = this.props;

		return answers.map((item) => <AnswerItem key={item._id} answer={item} postId={postId} />);
	}
}

AnswerFeed.propTypes = {
	answers: PropTypes.array.isRequired,
	postId: PropTypes.string.isRequired
};

export default AnswerFeed;
