import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PostItem from './PostItem';

class PostFeed extends Component {
	constructor(props) {
		super(props);

		this.state = {
			posts: this.props.posts
		};

		this.searchTable = this.searchTable.bind(this);
	}

	searchTable = (e) => {
		const string = e.target.value;
		const searchPrbatch = this.props.posts.filter((item) =>
			Object.keys(item.text).some((k) => item.text.toLowerCase().includes(string.toLowerCase()))
		);
		console.log('change', searchPrbatch);

		this.setState({
			posts: searchPrbatch
		});
	};

	render() {
		const { isAuth } = this.props.auth;
		const posts = this.state.posts;

		return (
			<div>
				{isAuth ? (
					<div className="md-form mt-0">
						<input class="form-control" type="text" placeholder="Search" onChange={this.searchTable} />
					</div>
				) : null}

				<br />
				{posts.map((item) => <PostItem key={item._id} post={item} />)}
			</div>
		);
	}
}

PostFeed.propTypes = {
	posts: PropTypes.object.isRequired,
	auth: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	auth: state.auth
});

export default connect(mapStateToProps)(PostFeed);
