const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//Load model
const Post = require('../../models/Post');

//Load Post validator
const validatePostInput = require('../../validation/post');

//@route GET api/posts
//@desc Create posts
//@access public
router.get('/', (req, res) => {
	Post.find()
		.sort({ date: -1 })
		.then((posts) => res.json(posts))
		.catch((err) => res.status(404).json({ noposts: 'no posts found' }));
});

//@route GET api/posts/:id
//@desc Get posts by id
//@access public
router.get('/:id', (req, res) => {
	Post.findById(req.params.id)
		.then((posts) => res.json(posts))
		.catch((err) => res.status(404).json({ nopostfound: 'no post found of this id' }));
});

//@route POST api/posts
//@desc Create posts
//@access Private
router.post('/', (req, res) => {
	const { errors, isValid } = validatePostInput(req.body);

	//check validation
	if (!isValid) {
		return res.status(400).json(errors);
	}

	const newPost = new Post({
		text: req.body.text,
		name: req.body.name,
		avatar: req.body.avatar,
		user: req.body.id
	});

	newPost.save().then((post) => res.json(post));
});

// @route   DELETE api/posts/:id
// @desc    Delete post
// @access  Private
router.delete('/:id', (req, res) => {
	Post.findByIdAndDelete(req.params.id)
		.then(() => {
			res.json({ success: true });
		})
		.catch((err) => res.status(404).json({ postnotfound: 'No post found' }));
});

// @route   POST api/posts/like/:id
// @desc    Like post
// @access  Private
router.post('/like/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
	Post.findById(req.params.id)
		.then((post) => {
			if (post.likes.filter((like) => like.user.toString() === req.user.id).length > 0) {
				return res.status(400).json({ alreadyliked: 'User already liked this post' });
			}

			// Add user id to likes array
			post.likes.unshift({ user: req.user.id });

			post.save().then((post) => res.json(post));
		})
		.catch((err) => res.status(404).json({ postnotfound: 'No post found' }));
});

// @route   POST api/posts/unlike/:id
// @desc    Unlike post
// @access  Private
router.post('/unlike/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
	Post.findById(req.params.id)
		.then((post) => {
			if (post.likes.filter((like) => like.user.toString() === req.user.id).length === 0) {
				return res.status(400).json({ notliked: 'You have not yet liked this post' });
			}

			// Get remove index
			const removeIndex = post.likes.map((item) => item.user.toString()).indexOf(req.user.id);

			// Splice out of array
			post.likes.splice(removeIndex, 1);

			// Save
			post.save().then((post) => res.json(post));
		})
		.catch((err) => res.status(404).json({ postnotfound: 'No post found' }));
});

// @route   POST api/posts/answer/:id
// @desc    Add answer to post
// @access  Private
router.post('/answer/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
	const { errors, isValid } = validatePostInput(req.body);

	// Check Validation
	if (!isValid) {
		// If any errors, send 400 with errors object
		return res.status(400).json(errors);
	}

	Post.findById(req.params.id)
		.then((post) => {
			const newAnswer = {
				text: req.body.text,
				name: req.body.name,
				avatar: req.body.avatar,
				user: req.user.id
			};

			// Add to answers array
			post.answers.unshift(newAnswer);

			// Save
			post.save().then((post) => res.json(post));
		})
		.catch((err) => res.status(404).json({ postnotfound: 'No post found' }));
});

// @route   DELETE api/posts/answer/:id/:answer_id
// @desc    Remove answer from post
// @access  Private
router.delete('/answer/:id/:answer_id', passport.authenticate('jwt', { session: false }), (req, res) => {
	Post.findById(req.params.id)
		.then((post) => {
			// Check to see if answer exists
			if (post.answers.filter((answer) => answer._id.toString() === req.params.answer_id).length === 0) {
				return res.status(404).json({ answernotexists: 'Answer does not exist' });
			}

			// Get remove index
			const removeIndex = post.answers.map((item) => item._id.toString()).indexOf(req.params.answer_id);

			// Splice answer out of array
			post.answers.splice(removeIndex, 1);

			post.save().then((post) => res.json(post));
		})
		.catch((err) => res.status(404).json({ postnotfound: 'No post found' }));
});

module.exports = router;
